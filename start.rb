# frozen_string_literal: true

# Check site
class Check
  attr_reader :from, :psw, :to, :urls

  require 'net/http'
  require 'mail'

  def initialize(*args)
    @from = args[0]
    @psw = args[1]
    @to = args[2]
    @urls = args[3]
    @status = urls.map { |_u| 200 }
  end

  def self.load_ursl
    File.open('urls.txt', &:read).split
  rescue StandardError => e
    p e
    exit
  end

  def send_mail(subj)
    options = { address: 'smtp.ukr.net',
                port: 465,
                domain: 'mail.ukr.net',
                user_name: from,
                password: psw,
                ssl: true }

    Mail.defaults { delivery_method :smtp, options }
    mail = Mail.new(to: to, from: from, subject: subj)
    mail.deliver!
  end

  def get_response(uri)
    response = Net::HTTP.start(uri.host, uri.port, use_ssl: uri.scheme == 'https') do |http|
      http.request(Net::HTTP::Get.new(uri))
    end
    response.code
  rescue StandardError
    0
  end

  def check_code(code, status)
    if status.zero?
      send_mail("#{url}: ERROR! Site unavailable!")
    elsif status == 200 && code != 200
      send_mail("#{url}: ERROR! Status #{code}")
    elsif status != 200 && code == 200
      send_mail("#{url}: OK! Status #{code}")
    end
  end

  def call
    urls.each_with_index do |url, index|
      code = get_response(URI(url)).to_i
      check_code(code, @status[index])
      @status[index] = code if code != @status[index]
    end
  end
end

if ARGV.length < 3 || ARGV.length > 3
  puts 'Check parameters!'
  puts 'Example: ruby start.rb rb@ukr.net password123 mailto@gmail.com'
  exit
end

$PROGRAM_NAME = 'ruby-check-my-site'

urls = Check.load_ursl
pid_file = File.open('pid.txt', 'w')

Process.daemon
pid = Process.pid

pid_file.write pid
pid_file.close

check_now = Check.new(ARGV[0], ARGV[1], ARGV[2], urls)

loop do
  check_now.call
  Signal.trap('INT') { exit }
  sleep 60
end
